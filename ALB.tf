provider "aws" {
    access_key = ""
    secret_key = ""
  
    profile = "default"
    region  = "ap-south-1"
}                              #initiating the provider

resource "aws_vpc" "alb_vpc"{
    cidr_block = "10.0.1.0/27"
    tags = {
        "Name" = "alb_vpc"
    }
}                              #creating vpc with desired cidr block
resource "aws_subnet" "alb_pub1" {
  vpc_id     = aws_vpc.alb_vpc.id
  cidr_block = "10.0.1.0/28"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "alb_pub1"
  }
}                           
resource "aws_subnet" "alb_pub2" {
  vpc_id     = aws_vpc.alb_vpc.id
  cidr_block = "10.0.1.16/28"
  availability_zone = "ap-south-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "alb_pub2"
  }                               #creating two subnet in various availability zone as this is ALB task
}
resource "aws_internet_gateway" "alb_igw" {
  vpc_id = aws_vpc.alb_vpc.id

  tags = {
    "Name" = "alb_igw"
  }                                          # creating igw for internet access
}
resource "aws_route_table" "alb_rtb" {
  vpc_id = aws_vpc.alb_vpc.id

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.alb_igw.id
  }

  route {
    cidr_block        = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.alb_igw.id
  }                                                  # rtb for the security purpose

  tags = {
    Name = "alb_rtb"
  }
}
resource "aws_route_table_association" "alb_rtb1" {
  subnet_id      = aws_subnet.alb_pub1.id
  route_table_id = aws_route_table.alb_rtb.id
}
resource "aws_route_table_association" "alb_rtb2"{
  subnet_id = aws_subnet.alb_pub2.id
  route_table_id = aws_route_table.alb_rtb.id
}                                                   #associating both the subnet with rtb
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.alb_vpc.id

    ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }


}                                #using the default sg and editing the inbound rules.

resource "aws_instance" "alb_EC21" {
    ami = "ami-0c1a7f89451184c8b"
    instance_type = "t2.micro"
    availability_zone = "ap-south-1a"
    subnet_id = aws_subnet.alb_pub1.id
    key_name   = "karthi-mumbai" 
      
    tags = {
      Name = "alb_EC21"
  }                                           #  creating instance with a webpage in subnet 1
  user_data = <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt-get -y install apache2         
                sudo systemctl start apache2
                touch /var/www/html/test.html
                sudo bash -c 'echo hello am running in aws server with alb and my name is ONE 1  HELLO > /var/www/html/index.html'
                EOF
}
resource "aws_instance" "alb_EC22" {
    ami = "ami-0c1a7f89451184c8b"
    instance_type = "t2.micro"
    availability_zone = "ap-south-1b"
    subnet_id = aws_subnet.alb_pub2.id
    key_name   = "karthi-mumbai"
  
    tags = {
      Name = "alb_EC22"
  }                                          #  creating instance with a webpage in subnet 2
  user_data = <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt-get -y install apache2
                sudo systemctl start apache2
                touch /var/www/html/test.html
                sudo bash -c 'echo hello am running in aws server with alb and my name is TWO 2   HI   > /var/www/html/index.html'
                EOF
}

resource "aws_lb_target_group" "alb_tg" {
  
  name     = "albtg"
  port     = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id   = aws_vpc.alb_vpc.id
}                           # creating target group for application load balancer


resource "aws_lb_target_group_attachment" "ec2_attach_1" {
  target_group_arn = aws_lb_target_group.alb_tg.arn
  target_id        = aws_instance.alb_EC21.id
  port             = 80
}                                # attaching the target 1 to the target group

resource "aws_lb_target_group_attachment" "ec2_attach_2" {
  target_group_arn = aws_lb_target_group.alb_tg.arn
  target_id        = aws_instance.alb_EC22.id
  port             = 80
}                               # attaching the target 2 to the target group

resource "aws_lb" "alb_g1" {
  name               = "test-alb"
  internal           = false
  load_balancer_type = "application"  
  subnet_mapping {
    subnet_id            = aws_subnet.alb_pub1.id
  }

  subnet_mapping {
    subnet_id            = aws_subnet.alb_pub2.id
  }
  enable_deletion_protection = false
  ip_address_type = "ipv4"
  tags = {
    Environment = "test"
  }                                  #creating lb --> application (ALB)
}
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb_g1.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }                                 # creating listener for the alb 
}


